--PRUEBAS PARA TRIGGERS---------------------------------------------------------------------------------------------------------
--1
--IntegranteComisionElectoral(numeroAfiliado, lema)
INSERT INTO IntegranteComisionElectoral VALUES (1, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (2, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (3, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (4, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (5, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (6, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (7, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (8, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (9, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (10, 'La numero uno');
INSERT INTO IntegranteComisionElectoral VALUES (11, 'La numero uno');
DELETE FROM IntegranteComisionElectoral WHERE lema = 'La numero uno';

--2
INSERT INTO IntegranteComisionFiscal VALUES (1, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (2, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (3, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (4, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (5, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (6, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (7, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (8, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (9, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (10, 'La numero uno');
INSERT INTO IntegranteComisionFiscal VALUES (11, 'La numero uno');
DELETE FROM IntegranteComisionFiscal WHERE lema = 'La numero uno';

--3
INSERT INTO IntegranteComisionDirectiva VALUES (1, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (2, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (3, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (4, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (5, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (6, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (7, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (8, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (9, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (10, 'La numero uno');
INSERT INTO IntegranteComisionDirectiva VALUES (11, 'La numero uno');
DELETE FROM IntegranteComisionDirectiva WHERE lema = 'La numero uno';

--4
--VotacionDirectiva(nombreVotacion, listaElectoral, listaFiscal, listaDirectiva)
INSERT INTO VotacionDirectiva VALUES ('Votacion directiva 1','La numero uno' , 'La numero uno','La numero uno');

--5
--ParticipacionVotante(numeroAfiliado, nombreVotacion)
INSERT INTO ParticipacionVotante VALUES(1, 'Votacion 1');

--7 
--Votacion(nombre, fechaInicio, fechaFin, estado)
INSERT INTO Votacion VALUES ('Votacion 1',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'HABILITADA');
INSERT INTO Votacion VALUES ('Votacion 3',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'HABILITADA');

DELETE FROM Votacion WHERE nombre = 'Votacion 3';

--8
--ParticipacionVotante(numeroAfiliado, nombreVotacion)
--Caso se encuentra atrasado
INSERT INTO ParticipacionVotante VALUES(1, 'Votacion 3');
INSERT INTO Votacion VALUES ('Votacion 3',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'HABILITADA');
INSERT INTO Cuota VALUES (1, TO_DATE('28-07-2018', 'DD-MM-YYYY'));
--Caso ultima modificacion es admin
DELETE FROM Votante WHERE numeroAfiliado = 1; 
INSERT INTO Votante VALUES (1, 'Usuario1', 'admin', 10987601, 97654301, 'Nombre', 'Apellido', 1);
UPDATE Votante SET usuarioUltimaModificacion = 'Usuario1' WHERE numeroAfiliado = 1;

--9
INSERT INTO Credenciales VALUES ('Mica123', 'contra3');
INSERT INTO Credenciales VALUES ('admin', 'adminadmin');
INSERT INTO Administrativo VALUES ('admin');
--Caso es administrador
INSERT INTO Votante VALUES (53, 'Mica123', 'admin', 12345678, 12345679, 'Micaela', 'Blanco', 1);
--Votante(numeroVotante, nombreUsuario, usuarioQueModifica, cedula, credencial, nombre, apellido)
--Caso no es administrador
INSERT INTO Votante VALUES (13, 'Usuario13', 'Usuario13', 10987613, 97654313, 'Nombre', 'Apellido', 1);

--10
--Votacion(nombre, fechaInicio, fechaFin, estado)
--De CERRADA a BORRADOR
INSERT INTO Votaciones VALUES ('Votacion 1',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'CERRADA');
UPDATE Votaciones SET estado = 'BORRADOR' WHERE nombre = 'Votacion 1';
DELETE FROM Votaciones WHERE nombre = 'Votacion 1';
--De CERRADA a HABILITADA
INSERT INTO Votaciones VALUES ('Votacion 2',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'CERRADA');
UPDATE Votaciones SET estado = 'HABILITADA' WHERE nombre = 'Votacion 2';
DELETE FROM Votaciones WHERE nombre = 'Votacion 2';
--De PUBLICADA a BORARDOR
INSERT INTO Votaciones VALUES ('Votacion 3',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'PUBLICADA');
UPDATE Votaciones SET estado = 'BORRADOR' WHERE nombre = 'Votacion 3';
DELETE FROM Votaciones WHERE nombre = 'Votacion 3';
--De PUBLICADA a HABILITADA
INSERT INTO Votaciones VALUES ('Votacion 4',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'PUBLICADA');
UPDATE Votaciones SET estado = 'HABILITADA' WHERE nombre = 'Votacion 4';
DELETE FROM Votaciones WHERE nombre = 'Votacion 4';

--11
--Cuota(numeroAfiliado, fecha)
INSERT INTO Credenciales VALUES ('Mica123', 'contra3');
INSERT INTO Votante VALUES (53, 'Mica123', 'Mica123', 12345678, 12345679, 'Micaela', 'Blanco', 1);
UPDATE Votante SET afiliado = 0 WHERE numeroAfiliado = 53;
INSERT INTO Cuota VALUES (53, TO_DATE('01-07-2018', 'DD-MM-YYYY'));
INSERT INTO Cuotas VALUES (53, TO_DATE('28-11-2018', 'DD-MM-YYYY'));

--12
--Votante(numeroVotante, nombreUsuario, usuarioQueModifica, cedula, credencial, nombre, apellido)
--Caso usuarioUltimaModificacion admin
INSERT INTO Votante VALUES (13, 'Usuario13', 'admin', 10987613, 97654313, 'Nombre', 'Apellido', 1);
UPDATE Votante SET nombre = 'nuevo', usuarioUltimaModificacion = 'admin'  WHERE numeroAfiliado = 13;
--Caso usuarioUltimaModificacion mismo usuario
UPDATE Votante SET nombre = 'nuevo2', usuarioUltimaModificacion = 'Usuario13'  WHERE numeroAfiliado = 13;
--Caso usuarioUltimaModificacion incorrecto
UPDATE Votante SET nombre = 'nuevo', usuarioUltimaModificacion = 'Usuario2'  WHERE numeroAfiliado = 13;

--14
INSERT INTO Votacion VALUES ('Votacion 1',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'HABILITADA');
DELETE FROM Votacion WHERE nombre = 'Votacion 1';

--15
--Credenciales(nombreUsuario, contrasena)
--Caso no existe desafiliacion y esta al dia con la cuota
UPDATE Credenciales SET contrasena = 'nueva4' WHERE nombreUsuario = 'Usuario1';
INSERT INTO Cuota VALUES (1, TO_DATE('28-11-2018', 'DD-MM-YYYY'));
DELETE FROM Votante WHERE numeroAfiliado = 1;
--Caso existe desafiliacion pero hay cuota luego de la desafiliacion
UPDATE Credenciales SET contrasena = 'nueva' WHERE nombreUsuario = 'Usuario2';
INSERT INTO Cuota VALUES (2, TO_DATE('28-12-2018', 'DD-MM-YYYY'));
--Desafiliacion(numeroAfiliado,fecha,motivo)
INSERT INTO Desafiliacion VALUES(2,TO_DATE('27-11-2018', 'DD-MM-YYYY'),'Vacaciones');
DELETE FROM Votante WHERE numeroAfiliado = 2;

--16
--Mocion(nombre, descripcion, votosSi, votosNo, votosEnBlanco
INSERT INTO Mocion Values('Votacion 3', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Votacion 1', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Votacion 2', 'Descripcion votacion', 0, 0 ,0);
UPDATE Mociones SET votosSi = 1  WHERE nombre = 'Votacion 3'; 
--Votacion(nombre, fechaInicio, fechaFin, estado )
INSERT INTO Votacion VALUES ('Votacion 3',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'HABILITADA');
INSERT INTO Votacion VALUES ('Votacion 1',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'BORRADOR');
INSERT INTO Votacion VALUES ('Votacion 2',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'BORRADOR');
--SeleccionMociones(nombreSeleccion)
INSERT INTO SeleccionMociones VALUES('Votacion 3');
DELETE FROM SeleccionMociones WHERE nombreSeleccion = 'Votacion 3';

--17
--Mocion(nombre, descripcion, votosSi, votosNo, votosEnBlanco
INSERT INTO Mocion Values('Mocion 1', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 2', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 3', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 4', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 5', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 6', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 7', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 8', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 9', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 10', 'Descripcion votacion', 0, 0 ,0);
INSERT INTO Mocion Values('Mocion 11', 'Descripcion votacion', 0, 0 ,0);
--Votacion(nombre, fechaInicio, fechaFin, estado)
INSERT INTO Votacion VALUES ('Votacion 3',TO_TIMESTAMP('10-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS') , TO_TIMESTAMP('20-01-2019 17:00:00','DD-MM-YYYY HH24:MI:SS'), 'HABILITADA');
--AprobacionMociones(nombreAprovacion)
INSERT INTO AprovacionMociones VALUES ('Votacion 3');
--MocionPorAprovar(nombreMocion, nombreAprovacion)
INSERT INTO MocionPorAprovar VALUES ('Mocion 1', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 2', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 3', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 4', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 5', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 6', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 7', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 8', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 9', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 10', 'Votacion 3');
INSERT INTO MocionPorAprovar VALUES ('Mocion 11', 'Votacion 3');