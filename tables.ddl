CREATE TABLE Credenciales(
	nombreUsuario VARCHAR(255) NOT NULL,
	contrasena VARCHAR(255) NOT NULL,
	PRIMARY KEY (nombreUsuario)
);

CREATE TABLE Votante(
	numeroAfiliado INT NOT NULL,
    nombreUsuario VARCHAR(255) UNIQUE,
    usuarioUltimaModificacion VARCHAR(255),
	ci NUMBER(8) NOT NULL UNIQUE,
	credencial VARCHAR(8) NOT NULL UNIQUE,
	nombre VARCHAR(45) NOT NULL,
	apellido VARCHAR(45) NOT NULL,
	afiliado NUMBER(1) NOT NULL CHECK (afiliado IN (0, 1)),
    PRIMARY KEY (numeroAfiliado),
    CONSTRAINT nombreUsuarioVotante FOREIGN KEY (nombreUsuario)
    REFERENCES Credenciales(nombreUsuario),
    CONSTRAINT usuarioUltimaModificacion FOREIGN KEY (nombreUsuario)
    REFERENCES Credenciales(nombreUsuario)
);

CREATE TABLE Cuota(
	numeroAfiliado INT NOT NULL,
	fecha DATE NOT NULL,
	PRIMARY KEY (numeroAfiliado, fecha),
	CONSTRAINT numeroAfiliadoCuota FOREIGN KEY (numeroAfiliado)
	REFERENCES Votante(numeroAfiliado)
);

CREATE TABLE Administrativo(
	nombreUsuario VARCHAR(255) UNIQUE NOT NULL,
	CONSTRAINT nombreUsuarioAdministrativo FOREIGN KEY (nombreUsuario)
	REFERENCES Credenciales (nombreUsuario)
);

CREATE TABLE Desafiliacion(
	numeroAfiliado INT NOT NULL,
	fecha DATE NOT NULL,
	motivo VARCHAR(255),
	PRIMARY KEY (numeroAfiliado, fecha),
	CONSTRAINT numeroAfiliadoDesafiliacion FOREIGN KEY (numeroAfiliado)
	REFERENCES Votante(numeroAfiliado)
);

CREATE TABLE Lista(
	lema VARCHAR(255),
	votosDirectivosSi NUMBER NOT NULL,
	votosDirectivosBlanco NUMBER NOT NULL,
	votosElectoralesSi NUMBER NOT NULL,
	votosElectoralesBlanco NUMBER NOT NULL,
	votosFiscalesSi NUMBER NOT NULL,
	votosFiscalesBlanco NUMBER NOT NULL,
	PRIMARY KEY (lema)
);

CREATE TABLE IntegranteComisionElectoral(
	numeroAfiliado INT NOT NULL,
	lema VARCHAR(255) NOT NULL,
    PRIMARY KEY (numeroAfiliado),
	CONSTRAINT numeroAfiliadoElectoral FOREIGN KEY (numeroAfiliado)
	REFERENCES Votante(numeroAfiliado),
	CONSTRAINT listaElectoral FOREIGN KEY (lema)
	REFERENCES Lista(lema)
);

CREATE TABLE IntegranteComisionFiscal(
	numeroAfiliado INT NOT NULL,
	lema VARCHAR(255) NOT NULL,
    PRIMARY KEY (numeroAfiliado),
	CONSTRAINT numeroAfiliadoFiscal FOREIGN KEY (numeroAfiliado)
	REFERENCES Votante(numeroAfiliado),
	CONSTRAINT listaFiscal FOREIGN KEY (lema)
	REFERENCES Lista(lema)
);

CREATE TABLE IntegranteComisionDirectiva(
	numeroAfiliado INT NOT NULL,
	lema VARCHAR(255) NOT NULL,
	PRIMARY KEY (numeroAfiliado),
	CONSTRAINT numeroAfiliadoDirectivo FOREIGN KEY (numeroAfiliado)
	REFERENCES Votante(numeroAfiliado),
	CONSTRAINT listaDirectiva FOREIGN KEY (lema)
	REFERENCES Lista(lema)
);

CREATE TABLE Votacion(
	nombre VARCHAR(255) NOT NULL,
	fechaInicio TIMESTAMP NOT NULL,
	fechaFin TIMESTAMP NOT NULL,
	estado VARCHAR(55) NOT NULL CHECK (estado IN ('HABILITADA', 'BORRADOR', 'CERRADA', 'PUBLICADA')),
	PRIMARY KEY (nombre)
);

CREATE TABLE ParticipacionVotante(
	numeroAfiliado INT NOT NULL UNIQUE,
	nombreVotacion VARCHAR(255) NOT NULL,
	PRIMARY KEY (numeroAfiliado, nombreVotacion),
	CONSTRAINT nombreParticipante FOREIGN KEY (numeroAfiliado)
	REFERENCES Votante(numeroAfiliado),
	CONSTRAINT votacionAfectada FOREIGN KEY (nombreVotacion)
	REFERENCES Votacion(nombre)
);

CREATE TABLE VotacionDirectiva(
	nombreVotacion VARCHAR(255) NOT NULL,
	listaElectoral VARCHAR(255) NOT NULL UNIQUE,
	listaFiscal VARCHAR(255) NOT NULL UNIQUE,
	listaDirectiva VARCHAR(255) NOT NULL UNIQUE,
	PRIMARY KEY (nombreVotacion, listaElectoral, listaFiscal, listaDirectiva),
	CONSTRAINT fechasVotacion FOREIGN KEY (nombreVotacion)
	REFERENCES Votacion(nombre),
	CONSTRAINT lemaElectoral FOREIGN KEY (listaElectoral)
	REFERENCES Lista(lema),
	CONSTRAINT lemaFiscal FOREIGN KEY (listaFiscal)
	REFERENCES Lista(lema),
	CONSTRAINT lemaDirectivo FOREIGN KEY (listaDirectiva)
	REFERENCES Lista(lema)
);


CREATE TABLE Mocion(
	nombre VARCHAR(255) NOT NULL,
	descripcion VARCHAR(255),
	votosSi NUMBER NOT NULL,
	votosNo NUMBER NOT NULL,
	votosEnBlanco NUMBER NOT NULL,
	PRIMARY KEY (nombre)
);

CREATE TABLE AprovacionMociones(
	nombreAprovacion VARCHAR(255) NOT NULL,
	PRIMARY KEY (nombreAprovacion),
	CONSTRAINT nombreAprovacion FOREIGN KEY (nombreAprovacion)
	REFERENCES Votacion(nombre)
);


CREATE TABLE MocionPorAprovar(
	nombreMocion VARCHAR(255) NOT NULL,
	nombreAprovacion VARCHAR(255) NOT NULL,
	PRIMARY KEY (nombreMocion, nombreAprovacion),
	CONSTRAINT nombreDeMocionPorAprovar FOREIGN KEY (nombreMocion)
	REFERENCES Mocion(nombre),
	CONSTRAINT nombreDeAprovacion FOREIGN KEY (nombreAprovacion)
	REFERENCES AprovacionMociones(nombreAprovacion)
);

CREATE TABLE SeleccionMociones(
	nombreSeleccion VARCHAR(255) NOT NULL,
	PRIMARY KEY (nombreSeleccion),
	CONSTRAINT fechaSeleccion FOREIGN KEY (nombreSeleccion)
	REFERENCES Votacion(nombre)
);

CREATE TABLE MocionPorSeleccionar(
	nombreMocion VARCHAR(255) NOT NULL,
	nombreSeleccion VARCHAR(255) NOT NULL,
	PRIMARY KEY (nombreMocion, nombreSeleccion),
	CONSTRAINT nombreDeMocionPorSeleccionar FOREIGN KEY (nombreMocion)
	REFERENCES Mocion(nombre),
	CONSTRAINT nombrePorSeleccionar FOREIGN KEY (nombreSeleccion)
	REFERENCES SeleccionMociones(nombreSeleccion)	
);

CREATE TABLE HistoricoABMVotacion(
	nombreVotacion VARCHAR(255) NOT NULL,
	fecha TIMESTAMP NOT NULL,
	numeroAfiliadoEncargado INT NOT NULL,
	PRIMARY KEY (nombreVotacion, fecha, numeroAfiliadoEncargado),
	CONSTRAINT nombreHistoricoVotacion FOREIGN KEY (nombreVotacion)
	REFERENCES Votacion(nombre),
	CONSTRAINT numeroAfiliadoHistorico FOREIGN KEY (numeroAfiliadoEncargado)
	REFERENCES IntegranteComisionElectoral(numeroAfiliado)	
);

CREATE TABLE HistoricoABMVotante(
	numeroAfiliado INT NOT NULL,
	nombreModificador VARCHAR(255) UNIQUE NOT NULL,
	fecha TIMESTAMP NOT NULL,
	PRIMARY KEY (numeroAfiliado, fecha),
	CONSTRAINT numeroAfiliadoABMAdmin FOREIGN KEY (numeroAfiliado)
	REFERENCES Votante(numeroAfiliado),
	CONSTRAINT nombreUsuarioABMAdmin FOREIGN KEY (nombreModificador)
	REFERENCES Credenciales(nombreUsuario)    
);

CREATE TABLE Evento (
	nombreEvento VARCHAR(255) NOT NULL,    
	PRIMARY KEY (nombreEvento),
	CONSTRAINT nombreVotacionAsociada FOREIGN KEY (nombreEvento)
	REFERENCES Votacion(nombre)
);

CREATE TABLE Compra (
	fecha DATE NOT NULL,
	moneda CHAR(3) NOT NULL CHECK (moneda IN ('UYU', 'USD')),	
	monto INT NOT NULL,
	nombreEvento VARCHAR(255) NOT NULL,
	nombreAdmin VARCHAR(255) UNIQUE NOT NULL,
	PRIMARY KEY (fecha, moneda, monto),
	CONSTRAINT nombreAdministrativoCompra FOREIGN KEY (nombreAdmin)
	REFERENCES Administrativo(nombreUsuario),	
	CONSTRAINT nombreEventoCompra FOREIGN KEY (nombreEvento)
	REFERENCES Evento(nombreEvento)
);

CREATE TABLE Hospedaje(
	numeroVotante INT NOT NULL,
	nombreEvento VARCHAR(255) NOT NULL,
	tipoHabitacion VARCHAR(55) NOT NULL CHECK (tipoHabitacion IN ('SIMPLE', 'DOBLE', 'TRIPLE', 'SUITE')),
	PRIMARY KEY (numeroVotante, nombreEvento),
	CONSTRAINT numeroVotanteHospedaje FOREIGN KEY (numeroVotante)
	REFERENCES Votante(numeroAfiliado),
	CONSTRAINT eventoHospedaje FOREIGN KEY (nombreEvento)
	REFERENCES Evento(nombreEvento)
);

CREATE Table Acompanante(
	numeroVotante INT NOT NULL,
	nombreEvento VARCHAR(255) NOT NULL,
	nombreAcompaniante VARCHAR(255) NOT NULL,
	PRIMARY KEY (numeroVotante, nombreEvento),
	CONSTRAINT acompananteHospedaje FOREIGN KEY (numeroVotante, nombreEvento)
	REFERENCES Hospedaje(numeroVotante, nombreEvento)
);

CREATE TABLE VotanteEvento(
	numeroVotante INT NOT NULL,
	nombreEvento VARCHAR(255) NOT NULL,	
	PRIMARY KEY (numeroVotante, nombreEvento),
	CONSTRAINT numeroVotanteEvento FOREIGN KEY (numeroVotante)
	REFERENCES Votante(numeroAfiliado),
	CONSTRAINT relacionVotanteEvento FOREIGN KEY (nombreEvento)
	REFERENCES Evento(nombreEvento)	
);


CREATE Table CambioMonedas(
	fecha DATE NOT NULL,
	dolar NUMBER NOT NULL,
	PRIMARY KEY (fecha)
);


CREATE Table VotacionProcesadaEnResumen(
	nombreVotacion VARCHAR(255) NOT NULL,
	totalVotos NUMBER,
	totalVotosDirectivos NUMBER,
	totalVotosElectorales NUMBER,
	totalVotosFiscales NUMBER,
	PRIMARY KEY (nombreVotacion),
	CONSTRAINT votacionProcesada FOREIGN KEY (nombreVotacion)
	REFERENCES Votacion(nombre)
);

CREATE Table DetalleVotacionProcesada(
	nombreVotacion VARCHAR(255) NOT NULL,
	nombreListaMocion VARCHAR(255) NOT NULL,
	votosDirectivosSi NUMBER,
	votosDirectivosBlanco NUMBER,
	votosElectoralesSi NUMBER,
	votosElectoralesBlanco NUMBER,
	votosFiscalesSi NUMBER,
	votosFiscalesBlanco NUMBER,
	votosMocionSi NUMBER,
	votosMocionNo NUMBER,
	votosMocionEnBlanco NUMBER,
	PRIMARY KEY (nombreVotacion, nombreListaMocion),
	CONSTRAINT detalleVotacionProcesada FOREIGN KEY (nombreVotacion)
	REFERENCES VotacionProcesadaEnResumen(nombreVotacion)
);


--________________Sobre el votante________________
--corroborar que en la creacion el usuario sea admin
--cuando lo creo la contra no puede ser nula
-- y que el usuario salga Inhabilitado, por ser primera vez
--en el update
--hay dos triggers, uno sobre el create (que tiene que ser un admin), 
--y otro sobre el update

--si me viene un update a un usuario, el que lo hace es votante
	--sale habilitado solo si la cambia, y no tiene cuotas atrasadas, y esta afiliado 
	--en el caso en que sale inhabilitado
--si lo hace un administrativo 
	--el usuario TIENE QUE SALIR INHABILITADO, para evitar robo de votos

--al cambiar la contrasena:
	--caso 1 (habilitado y cambio la contra)
	--no tengo que verificar mas nada

	--caso 2 (la cambio porque nunca tuve contra (es primera vez))

	--caso 3 (la cambio, pero estoy inhabilitado por deber cuotas)

	--caso 4 (capaz que nunca entre, y ademas debo cuotas)

	--caso 5 (estoy dado de baja)
		--puedo hacer que el sistema me patee

--Si modifico las cuotas tengo que verificar que no sea por el admin (primera vez)
	--de esta manera salgo inhabilitado



--es bueno tener un timestamp en cada tabla interesante, en donde pongo el user stamp,
--y un timestamp, el user para saber quien modifico, y el timestamp la fecha (tengo que ver donde pongo esto)
--el userstamp ya me viene por parametro (asumo eso), entonces ademas tengo que guardar un campo para saber
--quien fue el ultimo en tocar la tabla


--Si me da el error de mutacion, fijarme en las diapositivas


--Los integrantes de alguna comision no pueden pertenecer a otra comision


--VAMOS A ASUMIR QUE UNA MOCION NO PUEDE ESTAR EN UNA SElECCION DE MOCIONES Y UNA APROVACION AL MISMO TIEMPO



--CONSTRAINT usuarioUltimaModificacion FOREIGN KEY (nombreUsuario)
--    REFERENCES Credenciales(nombreUsuario)     falta provar esto