-- Servicio 1 -------------------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ControlDePadron AS
	cantidad NUMBER;
BEGIN
	cantidad := CantidadVotantesHabilitados(); 
	
	DBMS_OUTPUT.PUT_LINE('La cantidad de votantes habilitados es: ' || cantidad);
END ControlDePadron;


-- Servicio 2 -------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE CierreDeVotacion(numeroAfiliado IN VARCHAR, nombreVotacion IN VARCHAR) AS
	ES_INTEGRANTE NUMBER;
	TRUE NUMBER;
BEGIN
	TRUE := 1;
	ES_INTEGRANTE := EsIntegranteComisionElectoral(numeroAfiliado);
	
	IF ES_INTEGRANTE = TRUE
	THEN
		IF ExisteVotacionYSePuedeCerrar(nombreVotacion) = TRUE
		THEN
			CerrarVotacion(nombreVotacion); 
		ELSE 
			DBMS_OUTPUT.PUT_LINE('ERROR: no existe la votacion ingresada, o la misma no esta publicada.');	
		END IF;
	ELSE
		DBMS_OUTPUT.PUT_LINE('ERROR: quien realiza un cierre de votacion debe ser integrante de la comision electoral.');
	END IF;
END CierreDeVotacion;

-- Servicio 3 -------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE ConsultaDeVoto(numeroAfiliado IN NUMBER, nombreVotacion IN VARCHAR) AS
	voto NUMBER;
BEGIN
	SELECT COUNT(*)
	INTO voto
	FROM ParticipacionVotante p
	WHERE p.numeroAfiliado = numeroAfiliado AND p.nombreVotacion = nombreVotacion;

	IF voto = 0 
		THEN DBMS_OUTPUT.PUT_LINE('El votante no ha participado en: ' || nombreVotacion);
		ELSE DBMS_OUTPUT.PUT_LINE('El votante ha participado en: ' || nombreVotacion);
	END IF;

EXCEPTION
	WHEN NO_DATA_FOUND
	THEN DBMS_OUTPUT.PUT_LINE('ERROR: el votante no ha participado en: ' || nombreVotacion);

END ConsultaDeVoto;

-- Servicio 4 -------------------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE ResumenDeVotaciones(fechaInicio IN DATE, fechaFin IN DATE) AS
	CURSOR votacionesDirectivas IS 
		SELECT v.nombre, v.fechaFin, vp.totalVotos
		FROM  VotacionProcesadaEnResumen vp FULL OUTER JOIN Votacion v ON v.nombre = vp.nombreVotacion
		WHERE v.estado = 'CERRADA' OR v.estado = 'PUBLICADA'
		AND ((v.fechaInicio BETWEEN fechaInicio AND fechaFin)
		OR (v.fechaFin BETWEEN fechaInicio AND fechaFin))
		AND v.nombre IN (SELECT vd.nombreVotacion FROM VotacionDirectiva vd) 
		ORDER BY v.fechafin DESC;
	CURSOR seleccionMociones IS
		SELECT v.nombre, v.fechaFin, vp.totalVotos
		FROM  VotacionProcesadaEnResumen vp FULL OUTER JOIN Votacion v ON v.nombre = vp.nombreVotacion
		WHERE v.estado = 'CERRADA' OR v.estado = 'PUBLICADA'
		AND ((v.fechaInicio BETWEEN fechaInicio AND fechaFin)
		OR (v.fechaFin BETWEEN fechaInicio AND fechaFin))
		AND v.nombre IN (SELECT s.nombreSeleccion FROM SeleccionMociones s)
		ORDER BY v.fechafin DESC;
	CURSOR aprovacionMociones IS
		SELECT v.nombre, v.fechaFin, vp.totalVotos
		FROM  VotacionProcesadaEnResumen vp FULL OUTER JOIN Votacion v ON v.nombre = vp.nombreVotacion
		WHERE v.estado = 'CERRADA' OR v.estado = 'PUBLICADA'
		AND ((v.fechaInicio BETWEEN fechaInicio AND fechaFin)
		OR (v.fechaFin BETWEEN fechaInicio AND fechaFin))
		AND v.nombre IN (SELECT a.nombreAprovacion FROM AprovacionMociones a)
		ORDER BY v.fechafin DESC;
BEGIN
	DBMS_OUTPUT.PUT_LINE('##### Resumen votacion directiva #####');
	FOR votacionD IN votacionesDirectivas LOOP
	 	IF votacionD.totalVotos IS NOT NULL --se fija si ya lo tengo materializado
	 	THEN
	 		MostrarResumenGuardadoVD(votacionD.nombre, votacionD.fechaFin);
	 	ELSE
	 		MostrarResumenVotacionD(votacionD.nombre, votacionD.fechaFin);
	 	END IF;
	END LOOP;

	DBMS_OUTPUT.PUT_LINE('##### Resumen seleccion de mociones #####');
	FOR seleccionM IN seleccionMociones LOOP
		IF seleccionM.totalVotos IS NOT NULL --se fija si ya lo tengo materializado
		THEN
			MostrarResumenGuardadoSM(seleccionM.nombre, seleccionM.fechaFin);
		ELSE
			MostrarResumenSeleccionM(seleccionM.nombre, seleccionM.fechaFin);
		END IF;
	END LOOP;

	DBMS_OUTPUT.PUT_LINE('##### Resumen aprovacion de mociones #####');
	FOR aprovacionM IN aprovacionMociones LOOP
		IF aprovacionM.totalVotos IS NOT NULL --se fija si ya lo tengo materializado
		THEN
			MostrarResumenGuardadoAM(aprovacionM.nombre, aprovacionM.fechaFin);
		ELSE
			MostrarResumenAprovacionM(aprovacionM.nombre, aprovacionM.fechaFin);
		END IF;
	END LOOP;
END ResumenDeVotaciones;


-- Servicio 5 ------------------------------------------------------------------------------------- 

CREATE OR REPLACE PROCEDURE EliminacionVotacion(nombreVotacion IN VARCHAR, numeroAfiliado IN NUMBER) AS
	ES_INTEGRANTE NUMBER;
	TRUE NUMBER;
BEGIN
		TRUE := 1;
		ES_INTEGRANTE := EsIntegranteComisionElectoral(numeroAfiliado);
		--Integrande de la COMISION ELECTORAL
		IF ES_INTEGRANTE = TRUE
		THEN
			EliminarCascadaVotacion(nombreVotacion);
			DBMS_OUTPUT.PUT_LINE('Se ha eliminado la votacion correctamente.');
		ELSE
			DBMS_OUTPUT.PUT_LINE('ERROR: el numero de afiliado ingresado no corresponde con un integrante de la comision electoral');
		END IF;
END EliminacionVotacion;

-- Servicio 6 -------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE ControlDeGastos(fechaInicio IN DATE, fechaFin IN DATE) AS
	CURSOR eventos IS 
		SELECT e.nombreEvento, v.fechaInicio, v.fechaFin
		FROM Evento e, Votacion v
		WHERE e.nombreEvento = v.nombre 
		AND ((v.fechaInicio BETWEEN fechaInicio AND fechaFin)
			OR (v.fechaFin BETWEEN fechaInicio AND fechaFin))
		ORDER BY v.fechaInicio, v.fechafin;
	CURSOR cambios IS
		SELECT *
		FROM CambioMonedas cm
		WHERE cm.fecha BETWEEN fechaInicio AND fechaFin;
BEGIN
	DBMS_OUTPUT.PUT_LINE('##### Control de gastos por evento ##########################################');
	FOR eve IN eventos LOOP
	 DBMS_OUTPUT.PUT_LINE('		###### Para el evento '|| eve.nombreEvento || ' los gastos son:');
	 MostrarGastosEnPesos(eve.nombreEvento);
	 MostrarGastosEnDolares(eve.nombreEvento);
	END LOOP; 
	DBMS_OUTPUT.PUT_LINE('##### Fin del control de gastos ##########################################');

EXCEPTION
	WHEN NO_DATA_FOUND
	THEN DBMS_OUTPUT.PUT_LINE('ERROR: no existen eventos dentro del periodo seleccionado.');

END ControlDeGastos;

-- Servicio 7 -------------------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE DarDeBaja(numeroAfiliado IN NUMBER, posibleAdministrativo IN VARCHAR, motivo IN VARCHAR) AS
	FECHA_DE_HOY DATE;
	FALSE NUMBER;
	TRUE NUMBER;
	nombre VARCHAR(255);
BEGIN
	FECHA_DE_HOY := TO_DATE(SYSDATE, 'DD-MM-YYYY');
	FALSE := 0;
	TRUE := 1;

	IF EsAdministrativo(posibleAdministrativo) = TRUE AND SeBorraASiMismo(posibleAdministrativo, numeroAfiliado) = FALSE
	THEN
		INSERT INTO Desafiliacion VALUES (numeroAfiliado, FECHA_DE_HOY, motivo);
	
		UPDATE Votante v
		SET v.afiliado = FALSE
		WHERE v.numeroAfiliado = numeroAfiliado;

		--Lo borro de las posibles comisiones a las que pertenece
		DELETE IntegranteComisionElectoral i WHERE i.numeroAfiliado = numeroAfiliado;
		DELETE IntegranteComisionFiscal i WHERE i.numeroAfiliado = numeroAfiliado;
		DELETE IntegranteComisionDirectiva i WHERE i.numeroAfiliado = numeroAfiliado;

		SELECT v.nombre 
		INTO nombre
		FROM Votante v
		WHERE v.numeroAfiliado = numeroAfiliado;

		DBMS_OUTPUT.PUT_LINE(nombre || ' ha sido desafiliado correctamente.');
		COMMIT;
	ELSE
		DBMS_OUTPUT.PUT_LINE('ERROR, el usuario no es Administrativo, o esta tratando de borrarse a si mismo.');
	END IF;

EXCEPTION
	WHEN OTHERS 
	THEN DBMS_OUTPUT.PUT_LINE('Error inesperado, verifique el numero de afiliado, e intente mas tarde.');
END DarDeBaja;












-- Procedimientos y funciones auxiliares --------------------------------------------------------------------------------


CREATE OR REPLACE FUNCTION CantidadVotantesHabilitados
RETURN NUMBER 
AS
	MAXIMO_CUOTAS_ATRASADAS NUMBER; 
    FECHA_MES_ACTUAL DATE;
    TRUE NUMBER;
    cantidad NUMBER;
BEGIN
	TRUE := 1;
	MAXIMO_CUOTAS_ATRASADAS := 3;
	SELECT DISTINCT COUNT(v.nombreUsuario)
	INTO cantidad
    FROM Votante v
	WHERE v.usuarioUltimaModificacion = v.nombreUsuario
	AND v.afiliado = TRUE
	AND v.numeroAfiliado IN (
			SELECT c.numeroAfiliado
			FROM Cuota c 
			WHERE c.numeroAfiliado = v.numeroAfiliado
			AND c.fecha > ADD_MONTHS(SYSDATE, -MAXIMO_CUOTAS_ATRASADAS)
		);
	RETURN cantidad;	
EXCEPTION 
	WHEN NO_DATA_FOUND
	THEN cantidad := 0;

	RETURN cantidad;
END CantidadVotantesHabilitados;


CREATE OR REPLACE FUNCTION ExisteVotacionYSePuedeCerrar(nombreVotacion IN VARCHAR) 
RETURN NUMBER AS
	CONDICION NUMBER;
BEGIN

	SELECT COUNT(*)
	INTO CONDICION 
	FROM Votacion v 
	WHERE v.nombre = nombreVotacion AND estado = 'PUBLICADA';
	RETURN CONDICION;
EXCEPTION 
	WHEN NO_DATA_FOUND
	THEN CONDICION := 0;

	RETURN CONDICION;
END ExisteVotacionYSePuedeCerrar;


CREATE OR REPLACE PROCEDURE CerrarVotacion(nombreVotacion IN VARCHAR) AS
	TIPO_VOTACION VARCHAR(255);
BEGIN
	TIPO_VOTACION := ObtenerTipoVotacion(nombreVotacion);
	IF TIPO_VOTACION := 'DIRECTIVA' 
	THEN
		ConteoVotacionDirectiva(nombreVotacion);
	END IF;
	IF TIPO_VOTACION := 'SELECCION_MOCIONES'
	THEN
		ConteoSeleccionDeMociones(nombreVotacion);
	END IF;
	IF TIPO_VOTACION := 'APROVACION_MOCIONES'
	THEN
		ConteoAprovacionDeMociones(nombreVotacion);
	END IF;
	UPDATE Votacion SET estado = 'CERRADA' WHERE nombre = nombreVotacion;
COMMIT;
COMMIT;
END CerrarVotacion;

--Como precondicion se asume que la votacion ya existe.
CREATE OR REPLACE FUNCTION ObtenerTipoVotacion(nombreVotacion IN VARCHAR) 
RETURN VARCHAR AS
	DIRECTIVA NUMBER;
	APROVACION NUMBER;
	TRUE NUMBER;
BEGIN
	TRUE := 1;
	--Verifico si es directiva
	SELECT COUNT(*) INTO DIRECTIVA
	FROM VotacionDirectiva v
	WHERE v.nombreVotacion = nombreVotacion;
	IF DIRECTIVA = TRUE
	THEN
		RETURN 'DIRECTIVA';
	END IF;
	--Verifico si es una aprovacion de mociones
	SELECT COUNT(*) INTO APROVACION
	FROM AprovacionMociones
	WHERE nombreAprovacion = nombreVotacion;
	IF APROVACION = TRUE
	THEN
		RETURN 'APROVACION_MOCIONES';
	END IF;
	--Por defecto es una seleccion
	RETURN 'SELECCION_MOCIONES';
END ObtenerTipoVotacion;

CREATE OR REPLACE PROCEDURE ConteoVotacionDirectiva(nombre IN VARCHAR) AS
	CURSOR listasDirectivas IS
		SELECT DISTINCT l.lema AS lema, l.votosDirectivosSi AS votosSi, l.votosDirectivosBlanco AS votosEnBlanco
		FROM Lista l
		WHERE l.lema IN (
			SELECT v.listaDirectiva FROM VotacionDirectiva v
			WHERE v.nombreVotacion = nombre);
	CURSOR listasElectorales IS
		SELECT  DISTINCT l.lema AS lema, l.votosElectoralesSi AS votosSi, l.votosElectoralesBlanco AS votosEnBlanco
		FROM Lista l
		WHERE l.lema IN (
			SELECT v.listaElectoral FROM VotacionDirectiva v
			WHERE v.nombreVotacion = nombre);
	CURSOR listasFiscales IS
		SELECT l.lema AS lema, l.votosFiscalesSi AS votosSi, l.votosFiscalesBlanco AS votosEnBlanco
		FROM Lista l
		WHERE l.lema IN (
			SELECT v.listaFiscal FROM VotacionDirectiva v
			WHERE v.nombreVotacion = nombre);
BEGIN
	DBMS_OUTPUT.PUT_LINE('##### Cierre votacion directiva ######');

	DBMS_OUTPUT.PUT_LINE('	##### Conteo listas directivas - Votos por si - Votos en blanco - ####');
	FOR lista IN listasDirectivas LOOP
		DBMS_OUTPUT.PUT_LINE('	##### Lema: ' || lista.lema);
		DBMS_OUTPUT.PUT_LINE('	###############################  ' || lista.votosSi || '  ##########  ' || lista.votosEnBlanco || '  ####');
	END LOOP; 
	DBMS_OUTPUT.PUT_LINE('	##### Conteo listas electorales  - Votos por si - Votos en blanco - ####');
	FOR lista IN listasElectorales LOOP
		DBMS_OUTPUT.PUT_LINE('	##### Lema: ' || lista.lema);
		DBMS_OUTPUT.PUT_LINE('	###############################  ' || lista.votosSi || '  ##########  ' || lista.votosEnBlanco || '  ####');
	END LOOP;
	DBMS_OUTPUT.PUT_LINE('	##### Conteo listas fiscales - Votos por si - Votos en blanco - ####');
	FOR lista IN listasFiscales LOOP
		DBMS_OUTPUT.PUT_LINE('	##### Lema: ' || lista.lema);
		DBMS_OUTPUT.PUT_LINE('	###############################  ' || lista.votosSi || '  ##########  ' || lista.votosEnBlanco || '  ####');
	END LOOP;
	DBMS_OUTPUT.PUT_LINE('##### La votacion se ha cerrado correctamente ######');
END ConteoVotacionDirectiva;


CREATE OR REPLACE PROCEDURE ConteoSeleccionDeMociones(nombre IN VARCHAR) AS
	CURSOR mociones IS
		SELECT m.nombre AS nombreMocion, m.votosSi AS votosSi, m.votosNo AS votosNo, m.votosEnBlanco AS votosEnBlanco
		FROM MocionPorSeleccionar ma, Mocion m
		WHERE ma.nombreSeleccion = nombre
		AND ma.nombreMocion = m.nombre;
BEGIN
	DBMS_OUTPUT.PUT_LINE('##### Cierre seleccion de mociones ######');
	DBMS_OUTPUT.PUT_LINE('	##### Conteo mociones - Nombre mocion - Votos por si - Rechazos - Votos en blanco - ####');
	FOR mocion IN mociones LOOP
		DBMS_OUTPUT.PUT_LINE('	###############################  ' || mocion.nombreMocion 
			|| '  #########  ' || mocion.votosSi || '  ##########  ' || mocion.votosNo 
			|| '  ##########  ' || mocion.votosEnBlanco || '  ####');
	END LOOP;
	DBMS_OUTPUT.PUT_LINE('##### La votacion se ha cerrado correctamente ######');
END ConteoSeleccionDeMociones;

CREATE OR REPLACE PROCEDURE ConteoAprovacionDeMociones(nombre IN VARCHAR) AS
	CURSOR mociones IS
		SELECT m.nombre AS nombreMocion, m.votosSi AS votosSi, m.votosNo AS votosNo, m.votosEnBlanco AS votosEnBlanco
		FROM MocionPorAprovar ma, Mocion m
		WHERE ma.nombreAprovacion = nombre
		AND ma.nombreMocion = m.nombre;
BEGIN
	DBMS_OUTPUT.PUT_LINE('##### Cierre aprovacion de mociones ######');
	DBMS_OUTPUT.PUT_LINE('	##### Conteo mociones - Nombre mocion - Votos por si - Votos no - Votos en blanco - ####');
	FOR mocion IN mociones LOOP
		DBMS_OUTPUT.PUT_LINE('	###############################  ' || mocion.nombreMocion 
			|| '  #########  ' || mocion.votosSi || '  ##########  ' || mocion.votosNo 
			|| '  ##########  ' || mocion.votosEnBlanco || '  ####');
	END LOOP;
	DBMS_OUTPUT.PUT_LINE('##### La votacion se ha cerrado correctamente ######');
END ConteoAprovacionDeMociones;

CREATE OR REPLACE PROCEDURE EliminarCascadaVotacion(nombreVotacion IN VARCHAR) AS
BEGIN
	DELETE ParticipacionVotante pv WHERE pv.nombreVotacion = nombreVotacion;
	--Trato de borrar seleccion de mociones
	DELETE MocionPorSeleccionar WHERE nombreSeleccion = nombreVotacion;
	DELETE SeleccionMociones WHERE nombreSeleccion = nombreVotacion;
	--Trato de borrar aprovacion de mociones
	DELETE MocionPorAprovar WHERE nombreAprovacion = nombreVotacion;
	DELETE AprovacionMociones WHERE nombreAprovacion = nombreVotacion;
	--Trato de borrar votacion directiva
	DELETE Compra WHERE nombreEvento = nombreVotacion;
	DELETE Acompanante WHERE nombreEvento = nombreVotacion;
	DELETE Hospedaje WHERE nombreEvento = nombreVotacion;
	DELETE VotanteEvento WHERE nombreEvento = nombreVotacion;
	DELETE Evento WHERE nombreEvento = nombreVotacion;
	DELETE VotacionDirectiva v WHERE v.nombreVotacion = nombreVotacion; 
	--Borro la votacion en la cima de la jerarquia
	DELETE Votacion WHERE nombre = nombreVotacion;
COMMIT;
END EliminarCascadaVotacion;

CREATE OR REPLACE FUNCTION EsIntegranteComisionElectoral(numeroAfiliado IN NUMBER)
	RETURN NUMBER 
AS
	ES_INTEGRANTE NUMBER;
BEGIN
	SELECT COUNT(*) INTO ES_INTEGRANTE
	FROM IntegranteComisionElectoral i
	WHERE i.numeroAfiliado = numeroAfiliado;
	RETURN ES_INTEGRANTE;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN RETURN 0;
END EsIntegranteComisionElectoral;


CREATE OR REPLACE PROCEDURE MostrarGastosEnPesos(nombreEvento IN VARCHAR) AS
	CURSOR compras IS
		SELECT c.fecha AS fecha, c.moneda 
			AS tipoMoneda, c.monto AS monto, cm.dolar AS valorDolar  
		FROM Compra c LEFT JOIN CambioMonedas cm
		ON c.fecha = cm.fecha
		WHERE c.nombreEvento = nombreEvento AND c.moneda = 'UYU'
		ORDER BY c.fecha;
	contador NUMBER;
BEGIN
	contador := 0;
	FOR comp IN compras LOOP
		contador := contador + 1;
		IF comp.valorDolar IS NULL THEN
			DBMS_OUTPUT.PUT_LINE('		###### En la fecha '  || comp.fecha || ' se gasto: '
				|| comp.monto || ' en pesos, y no se tiene informacion del cambio a dolar como para mostrar su equivalente en la fecha.');
		ELSE
			DBMS_OUTPUT.PUT_LINE('		###### En la fecha '  || comp.fecha || ' se gasto: '
				|| comp.monto || ' en pesos, y ' || comp.monto/comp.valorDolar || ' su equivalente en dolares para la fecha.');
		END IF;
	END LOOP;

	IF contador = 0 THEN
		DBMS_OUTPUT.PUT_LINE('		###### No hay gastos en pesos.');
	END IF;
END MostrarGastosEnPesos;

CREATE OR REPLACE PROCEDURE MostrarGastosEnDolares(nombreEvento IN VARCHAR) AS
	CURSOR compras IS
		SELECT c.fecha AS fecha, c.moneda 
			AS tipoMoneda, c.monto AS monto, cm.dolar AS valorDolar  
		FROM Compra c LEFT JOIN CambioMonedas cm
		ON c.fecha = cm.fecha
		WHERE c.nombreEvento = nombreEvento AND c.moneda = 'USD'
		ORDER BY c.fecha;
	contador NUMBER;
BEGIN
	contador := 0;
	FOR comp IN compras LOOP
		contador := contador + 1;
		IF comp.valorDolar IS NULL THEN
			DBMS_OUTPUT.PUT_LINE('		###### En la fecha '  || comp.fecha || ' se gasto: '
				|| comp.monto || ' en dolares, y no se tiene informacion del valor del dolar como para mostrar su equivalente en pesos.');
		ELSE
			DBMS_OUTPUT.PUT_LINE('		###### En la fecha '  || comp.fecha || ' se gasto: '
				|| comp.monto || ' en dolares, y  ' || comp.monto*comp.valorDolar || ' su equivalente en pesos para la fecha.');
		END IF;
	END LOOP;

	IF contador = 0 THEN
		DBMS_OUTPUT.PUT_LINE('		###### No hay gastos en dolares.');
	END IF;
END MostrarGastosEnDolares;


CREATE OR REPLACE FUNCTION EsAdministrativo(posibleUsuarioAdmin IN VARCHAR)
	RETURN NUMBER 
AS
	ES_ADMIN NUMBER;
BEGIN
	SELECT COUNT(*) INTO ES_ADMIN
	FROM Administrativo
	WHERE nombreUsuario = posibleUsuarioAdmin;
	RETURN ES_ADMIN;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN RETURN 0;
END EsAdministrativo;

--Dado un usuario, y un afiliado, dice si corresponden a la misma persona
CREATE OR REPLACE FUNCTION SeBorraASiMismo(usuarioAdmin IN VARCHAR, afiliadoABorrar IN NUMBER)
	RETURN NUMBER 
AS
	SE_BORRA NUMBER;
BEGIN
	SELECT COUNT(*) INTO SE_BORRA
	FROM Votante
	WHERE nombreUsuario = usuarioAdmin AND numeroAfiliado = afiliadoABorrar;
	RETURN SE_BORRA;
EXCEPTION
	WHEN NO_DATA_FOUND
	THEN RETURN 0;
END SeBorraASiMismo;




-- Funciones auxiliares del servicio 4

--  Mostrar info guardada ------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE MostrarResumenGuardadoVD(nombreVotacion IN VARCHAR, fechaFin IN DATE) AS
	TOTAL_VOTOS_DIRECTIVOS NUMBER;
	TOTAL_VOTOS_ELECTORALES NUMBER;
	TOTAL_VOTOS_FISCALES NUMBER;
	CURSOR detallesDirectivos IS
		SELECT dp.nombreListaMocion AS lemaLista, dp.votosDirectivosSi AS votosSi, dp.votosDirectivosBlanco AS votosEnBlanco 
		FROM DetalleVotacionProcesada dp
		WHERE dp.nombreVotacion = nombreVotacion;
	CURSOR detallesElectorales IS
		SELECT dp.nombreListaMocion AS lemaLista, dp.votosElectoralesSi AS votosSi, dp.votosElectoralesBlanco AS votosEnBlanco 
		FROM DetalleVotacionProcesada dp
		WHERE dp.nombreVotacion = nombreVotacion;
	CURSOR detallesFiscales IS
		SELECT dp.nombreListaMocion AS lemaLista, dp.votosFiscalesSi AS votosSi, dp.votosFiscalesBlanco AS votosEnBlanco 
		FROM DetalleVotacionProcesada dp
		WHERE dp.nombreVotacion = nombreVotacion;
BEGIN
	SELECT vp.totalVotosDirectivos INTO TOTAL_VOTOS_DIRECTIVOS
	FROM VotacionProcesadaEnResumen vp
	WHERE vp.nombreVotacion = nombreVotacion;

	SELECT vp.totalVotosElectorales INTO TOTAL_VOTOS_ELECTORALES
	FROM VotacionProcesadaEnResumen vp
	WHERE vp.nombreVotacion = nombreVotacion;

	SELECT vp.totalVotosFiscales INTO TOTAL_VOTOS_FISCALES
	FROM VotacionProcesadaEnResumen vp
	WHERE vp.nombreVotacion = nombreVotacion;	

	DBMS_OUTPUT.PUT_LINE('#### Votacion directiva : ' || nombreVotacion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas directivas #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN detallesDirectivos LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_DIRECTIVOS)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_DIRECTIVOS) || ' % -  ###');
	END LOOP;

	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas electorales #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN detallesElectorales LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_ELECTORALES)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_ELECTORALES) || ' % -  ###');
	END LOOP;
	
	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas fiscales #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN detallesFiscales LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_FISCALES)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_FISCALES) || ' % -  ###');
	END LOOP;
END MostrarResumenGuardadoVD;



CREATE OR REPLACE PROCEDURE MostrarResumenGuardadoSM(nombreSeleccion IN VARCHAR, fechaFin IN DATE) AS
	CURSOR detalles IS
		SELECT nombreListaMocion AS nombreMocion, votosMocionSi AS votosSi, votosMocionNo AS votosNo, votosMocionEnBlanco AS votosEnBlanco
		FROM DetalleVotacionProcesada
		WHERE nombreVotacion = nombreSeleccion;
	CANTIDAD_TOTAL_VOTOS NUMBER;
BEGIN
	SELECT totalVotos INTO CANTIDAD_TOTAL_VOTOS 
	FROM VotacionProcesadaEnResumen 
	WHERE nombreVotacion = nombreSeleccion;

	DBMS_OUTPUT.PUT_LINE('#### Seleccion : ' || nombreSeleccion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Nombre mocion - Votos por si / %  - Votos de rechazo / % - Votos en blanco / % - ###');
	FOR detalle IN detalles LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.nombreMocion || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/CANTIDAD_TOTAL_VOTOS) 
			|| ' % - ' || detalle.votosNo || ' / ' || (detalle.votosNo/CANTIDAD_TOTAL_VOTOS)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/CANTIDAD_TOTAL_VOTOS) || ' % -  ###');
	END LOOP;
END MostrarResumenGuardadoSM;



CREATE OR REPLACE PROCEDURE MostrarResumenGuardadoAM(nombreAprovacion IN VARCHAR, fechaFin IN DATE) AS
	CURSOR detalles IS
		SELECT nombreListaMocion AS nombreMocion, votosMocionSi AS votosSi, votosMocionNo AS votosNo, votosMocionEnBlanco AS votosEnBlanco
		FROM DetalleVotacionProcesada
		WHERE nombreVotacion = nombreAprovacion;
	CANTIDAD_TOTAL_VOTOS NUMBER;
BEGIN
	SELECT totalVotos INTO CANTIDAD_TOTAL_VOTOS 
	FROM VotacionProcesadaEnResumen 
	WHERE nombreVotacion = nombreAprovacion;

	DBMS_OUTPUT.PUT_LINE('#### Aprovacion : ' || nombreAprovacion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Nombre mocion - Votos por si / %  - Votos de rechazo / % - Votos en blanco / % - ###');
	FOR detalle IN detalles LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.nombreMocion || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/CANTIDAD_TOTAL_VOTOS) 
			|| ' % - ' || detalle.votosNo || ' / ' || (detalle.votosNo/CANTIDAD_TOTAL_VOTOS)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/CANTIDAD_TOTAL_VOTOS) || ' % -  ###');
	END LOOP;
END MostrarResumenGuardadoAM;




--  Crear la info  ------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE MostrarResumenVotacionD(nombreVotacion IN VARCHAR, fechaFin IN DATE) AS
	TOTAL_VOTOS_DIRECTIVOS NUMBER;
	TOTAL_VOTOS_ELECTORALES NUMBER;
	TOTAL_VOTOS_FISCALES NUMBER;

	CURSOR listasDirectivas IS
		SELECT v.listaDirectiva as lemaLista, l.votosDirectivosSi AS votosSi, l.votosDirectivosBlanco AS votosEnBlanco
		FROM VotacionDirectiva v, Lista l
		WHERE v.nombreVotacion = nombreVotacion
		AND l.lema = v.listaDirectiva;
	CURSOR listasElectorales IS
		SELECT v.listaElectoral as lemaLista, l.votosElectoralesSi AS votosSi, l.votosElectoralesBlanco AS votosEnBlanco
		FROM VotacionDirectiva v, Lista l
		WHERE v.nombreVotacion = nombreVotacion
		AND l.lema = v.listaElectoral;
	CURSOR listasFiscales IS
		SELECT v.listaFiscal as lemaLista, l.votosFiscalesSi AS votosSi, l.votosFiscalesBlanco AS votosEnBlanco
		FROM VotacionDirectiva v, Lista l
		WHERE v.nombreVotacion = nombreVotacion
		AND l.lema = v.listaFiscal;
BEGIN
	SELECT COALESCE(SUM(l.votosDirectivosSi + l.votosDirectivosBlanco), 0) INTO TOTAL_VOTOS_DIRECTIVOS
	FROM VotacionDirectiva v, Lista l
	WHERE v.nombreVotacion = nombreVotacion
	AND l.lema = v.listaDirectiva;
	SELECT COALESCE(SUM(l.votosElectoralesSi + l.votosElectoralesBlanco), 0) INTO TOTAL_VOTOS_ELECTORALES
	FROM VotacionDirectiva v, Lista l
	WHERE v.nombreVotacion = nombreVotacion
	AND l.lema = v.listaElectoral;
	SELECT COALESCE(SUM(l.votosFiscalesSi + l.votosFiscalesBlanco), 0) INTO TOTAL_VOTOS_FISCALES
	FROM VotacionDirectiva v, Lista l
	WHERE v.nombreVotacion = nombreVotacion
	AND l.lema = v.listaFiscal;

	DBMS_OUTPUT.PUT_LINE('#### Votacion directiva : ' || nombreVotacion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas directivas #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN listasDirectivas LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_DIRECTIVOS)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_DIRECTIVOS) || ' % -  ###');
	END LOOP;

	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas electorales #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN listasElectorales LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_ELECTORALES)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_ELECTORALES) || ' % -  ###');
	END LOOP;
	
	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas fiscales #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN listasFiscales LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_FISCALES)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_FISCALES) || ' % -  ###');
	END LOOP;
COMMIT;
END MostrarResumenVotacionD;



CREATE OR REPLACE PROCEDURE MostrarResumenSeleccionM(nombreSeleccion IN VARCHAR, fechaFin IN DATE) AS
	TOTAL_VOTOS NUMBER;
	CURSOR mociones IS
		SELECT m.nombre AS nombre , m.votosSi AS votosSi, m.votosNo AS votosNo, m.votosEnBlanco AS votosEnBlanco
		FROM Mocion m, MocionPorSeleccionar ma
		WHERE ma.nombreSeleccion = nombreSeleccion AND m.nombre = ma.nombreMocion;
BEGIN 
	TOTAL_VOTOS := ObtenerTotalVotosSeleccion(nombreSeleccion);

	DBMS_OUTPUT.PUT_LINE('#### Seleccion : ' || nombreSeleccion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Nombre mocion - Votos aprovacion / %  - Votos de rechazo / % - Votos en blanco / % - ###');
	FOR mocion IN mociones LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || mocion.nombre || ' - ' || mocion.votosSi || ' / ' || (mocion.votosSi/TOTAL_VOTOS) 
			|| ' % - ' || mocion.votosNo || ' / ' || (mocion.votosNo/TOTAL_VOTOS)
			|| ' % - ' || mocion.votosEnBlanco || ' / ' || (mocion.votosEnBlanco/TOTAL_VOTOS) || ' % -  ###');
		INSERT INTO DetalleVotacionProcesada VALUES (nombreSeleccion, mocion.nombre, 0, 0, 0, 0, 0, 0, mocion.votosSi, mocion.votosNo, mocion.votosEnBlanco);
	END LOOP;
COMMIT;
END MostrarResumenSeleccionM;

CREATE OR REPLACE FUNCTION ObtenerTotalVotosSeleccion(nombreSeleccion IN VARCHAR) 
RETURN NUMBER AS
	CURSOR totalesVotos IS
		SELECT COALESCE((m.votosSi + m.votosNo + m.votosEnBlanco), 0) AS totalVotos
		FROM Mocion m, MocionPorSeleccionar ma
		WHERE ma.nombreSeleccion = nombreSeleccion AND m.nombre = ma.nombreMocion; 
BEGIN
	FOR total IN totalesVotos LOOP
		RETURN total.totalVotos;
	END LOOP;
	RETURN 0;
END ObtenerTotalVotosSeleccion;

CREATE OR REPLACE PROCEDURE MostrarResumenAprovacionM(nombreAprovacion IN VARCHAR, fechaFin IN DATE) AS
	CANTIDAD_VOTOS_SI NUMBER;
	CANTIDAD_VOTOS_NO NUMBER;
	CANTIDAD_VOTOS_BLANCO NUMBER;
	TOTAL_VOTOS NUMBER;
	CURSOR mociones IS
		SELECT m.nombre AS nombre , m.votosSi AS votosSi, m.votosNo AS votosNo, m.votosEnBlanco AS votosEnBlanco
		FROM Mocion m, MocionPorAprovar ma
		WHERE ma.nombreAprovacion = nombreAprovacion AND m.nombre = ma.nombreMocion;
BEGIN
	SELECT COALESCE(SUM(m.votosSi), 0) INTO CANTIDAD_VOTOS_SI
	FROM Mocion m, MocionPorAprovar ma
	WHERE ma.nombreAprovacion = nombreAprovacion AND ma.nombreMocion = m.nombre;
	
	SELECT COALESCE(SUM(m.votosNo), 0) INTO CANTIDAD_VOTOS_NO
	FROM Mocion m, MocionPorAprovar ma
	WHERE ma.nombreAprovacion = nombreAprovacion AND ma.nombreMocion = m.nombre;

	SELECT COALESCE(SUM(m.votosEnBlanco), 0) INTO CANTIDAD_VOTOS_BLANCO
	FROM Mocion m, MocionPorAprovar ma
	WHERE ma.nombreAprovacion = nombreAprovacion AND ma.nombreMocion = m.nombre;

	TOTAL_VOTOS := CANTIDAD_VOTOS_SI + CANTIDAD_VOTOS_NO + CANTIDAD_VOTOS_BLANCO;
	INSERT INTO VotacionProcesadaEnResumen VALUES (nombreAprovacion, TOTAL_VOTOS, 0, 0, 0);

	DBMS_OUTPUT.PUT_LINE('#### Aprovacion : ' || nombreAprovacion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Nombre mocion - Votos por si / %  - Votos de rechazo / % - Votos en blanco / % - ###');
	FOR mocion IN mociones LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || mocion.nombre || ' - ' || mocion.votosSi || ' / ' || (mocion.votosSi/TOTAL_VOTOS) 
			|| ' % - ' || mocion.votosNo || ' / ' || (mocion.votosNo/TOTAL_VOTOS)
			|| ' % - ' || mocion.votosEnBlanco || ' / ' || (mocion.votosEnBlanco/TOTAL_VOTOS) || ' % -  ###');
		INSERT INTO DetalleVotacionProcesada VALUES (nombreAprovacion, mocion.nombre, 0, 0, 0, 0, 0, 0, mocion.votosSi, mocion.votosNo, mocion.votosEnBlanco);
	END LOOP;
COMMIT;
END MostrarResumenAprovacionM;

