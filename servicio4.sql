--  Mostrar info guardada ------------------------------------------------------------------------
CREATE OR REPLACE PROCEDURE MostrarResumenGuardadoVD(nombreVotacion IN VARCHAR, fechaFin IN DATE) AS
	TOTAL_VOTOS_DIRECTIVOS NUMBER;
	TOTAL_VOTOS_ELECTORALES NUMBER;
	TOTAL_VOTOS_FISCALES NUMBER;
	CURSOR detallesDirectivos IS
		SELECT dp.nombreListaMocion AS lemaLista, dp.votosDirectivosSi AS votosSi, dp.votosDirectivosBlanco AS votosEnBlanco 
		FROM DetalleVotacionProcesada dp
		WHERE dp.nombreVotacion = nombreVotacion;
	CURSOR detallesElectorales IS
		SELECT dp.nombreListaMocion AS lemaLista, dp.votosElectoralesSi AS votosSi, dp.votosElectoralesBlanco AS votosEnBlanco 
		FROM DetalleVotacionProcesada dp
		WHERE dp.nombreVotacion = nombreVotacion;
	CURSOR detallesFiscales IS
		SELECT dp.nombreListaMocion AS lemaLista, dp.votosFiscalesSi AS votosSi, dp.votosFiscalesBlanco AS votosEnBlanco 
		FROM DetalleVotacionProcesada dp
		WHERE dp.nombreVotacion = nombreVotacion;
BEGIN
	SELECT vp.totalVotosDirectivos INTO TOTAL_VOTOS_DIRECTIVOS
	FROM VotacionProcesadaEnResumen vp
	WHERE vp.nombreVotacion = nombreVotacion;

	SELECT vp.totalVotosElectorales INTO TOTAL_VOTOS_ELECTORALES
	FROM VotacionProcesadaEnResumen vp
	WHERE vp.nombreVotacion = nombreVotacion;

	SELECT vp.totalVotosFiscales INTO TOTAL_VOTOS_FISCALES
	FROM VotacionProcesadaEnResumen vp
	WHERE vp.nombreVotacion = nombreVotacion;	

	DBMS_OUTPUT.PUT_LINE('#### Votacion directiva : ' || nombreVotacion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas directivas #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN detallesDirectivos LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_DIRECTIVOS)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_DIRECTIVOS) || ' % -  ###');
	END LOOP;

	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas electorales #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN detallesElectorales LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_ELECTORALES)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_ELECTORALES) || ' % -  ###');
	END LOOP;
	
	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas fiscales #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN detallesFiscales LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_FISCALES)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_FISCALES) || ' % -  ###');
	END LOOP;
END MostrarResumenGuardadoVD;



CREATE OR REPLACE PROCEDURE MostrarResumenGuardadoSM(nombreSeleccion IN VARCHAR, fechaFin IN DATE) AS
	CURSOR detalles IS
		SELECT nombreListaMocion AS nombreMocion, votosMocionSi AS votosSi, votosMocionNo AS votosNo, votosMocionEnBlanco AS votosEnBlanco
		FROM DetalleVotacionProcesada
		WHERE nombreVotacion = nombreSeleccion;
	CANTIDAD_TOTAL_VOTOS NUMBER;
BEGIN
	SELECT totalVotos INTO CANTIDAD_TOTAL_VOTOS 
	FROM VotacionProcesadaEnResumen 
	WHERE nombreVotacion = nombreSeleccion;

	DBMS_OUTPUT.PUT_LINE('#### Seleccion : ' || nombreSeleccion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Nombre mocion - Votos por si / %  - Votos de rechazo / % - Votos en blanco / % - ###');
	FOR detalle IN detalles LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.nombreMocion || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/CANTIDAD_TOTAL_VOTOS) 
			|| ' % - ' || detalle.votosNo || ' / ' || (detalle.votosNo/CANTIDAD_TOTAL_VOTOS)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/CANTIDAD_TOTAL_VOTOS) || ' % -  ###');
	END LOOP;
END MostrarResumenGuardadoSM;



CREATE OR REPLACE PROCEDURE MostrarResumenGuardadoAM(nombreAprovacion IN VARCHAR, fechaFin IN DATE) AS
	CURSOR detalles IS
		SELECT nombreListaMocion AS nombreMocion, votosMocionSi AS votosSi, votosMocionNo AS votosNo, votosMocionEnBlanco AS votosEnBlanco
		FROM DetalleVotacionProcesada
		WHERE nombreVotacion = nombreAprovacion;
	CANTIDAD_TOTAL_VOTOS NUMBER;
BEGIN
	SELECT totalVotos INTO CANTIDAD_TOTAL_VOTOS 
	FROM VotacionProcesadaEnResumen 
	WHERE nombreVotacion = nombreAprovacion;

	DBMS_OUTPUT.PUT_LINE('#### Aprovacion : ' || nombreAprovacion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Nombre mocion - Votos por si / %  - Votos de rechazo / % - Votos en blanco / % - ###');
	FOR detalle IN detalles LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.nombreMocion || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/CANTIDAD_TOTAL_VOTOS) 
			|| ' % - ' || detalle.votosNo || ' / ' || (detalle.votosNo/CANTIDAD_TOTAL_VOTOS)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/CANTIDAD_TOTAL_VOTOS) || ' % -  ###');
	END LOOP;
END MostrarResumenGuardadoAM;




--  Crear la info  ------------------------------------------------------------------------

CREATE OR REPLACE PROCEDURE MostrarResumenVotacionD(nombreVotacion IN VARCHAR, fechaFin IN DATE) AS
	TOTAL_VOTOS_DIRECTIVOS NUMBER;
	TOTAL_VOTOS_ELECTORALES NUMBER;
	TOTAL_VOTOS_FISCALES NUMBER;

	CURSOR listasDirectivas IS
		SELECT v.listaDirectiva as lemaLista, l.votosDirectivosSi AS votosSi, l.votosDirectivosBlanco AS votosEnBlanco
		FROM VotacionDirectiva v, Lista l
		WHERE v.nombreVotacion = nombreVotacion
		AND l.lema = v.listaDirectiva;
	CURSOR listasElectorales IS
		SELECT v.listaElectoral as lemaLista, l.votosElectoralesSi AS votosSi, l.votosElectoralesBlanco AS votosEnBlanco
		FROM VotacionDirectiva v, Lista l
		WHERE v.nombreVotacion = nombreVotacion
		AND l.lema = v.listaElectoral;
	CURSOR listasFiscales IS
		SELECT v.listaFiscal as lemaLista, l.votosFiscalesSi AS votosSi, l.votosFiscalesBlanco AS votosEnBlanco
		FROM VotacionDirectiva v, Lista l
		WHERE v.nombreVotacion = nombreVotacion
		AND l.lema = v.listaFiscal;
BEGIN
	SELECT COALESCE(SUM(l.votosDirectivosSi + l.votosDirectivosBlanco), 0) INTO TOTAL_VOTOS_DIRECTIVOS
	FROM VotacionDirectiva v, Lista l
	WHERE v.nombreVotacion = nombreVotacion
	AND l.lema = v.listaDirectiva;
	SELECT COALESCE(SUM(l.votosElectoralesSi + l.votosElectoralesBlanco), 0) INTO TOTAL_VOTOS_ELECTORALES
	FROM VotacionDirectiva v, Lista l
	WHERE v.nombreVotacion = nombreVotacion
	AND l.lema = v.listaElectoral;
	SELECT COALESCE(SUM(l.votosFiscalesSi + l.votosFiscalesBlanco), 0) INTO TOTAL_VOTOS_FISCALES
	FROM VotacionDirectiva v, Lista l
	WHERE v.nombreVotacion = nombreVotacion
	AND l.lema = v.listaFiscal;

	DBMS_OUTPUT.PUT_LINE('#### Votacion directiva : ' || nombreVotacion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas directivas #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN listasDirectivas LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_DIRECTIVOS)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_DIRECTIVOS) || ' % -  ###');
	END LOOP;

	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas electorales #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN listasElectorales LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_ELECTORALES)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_ELECTORALES) || ' % -  ###');
	END LOOP;
	
	DBMS_OUTPUT.PUT_LINE('	#### Detalle listas fiscales #### ');
	DBMS_OUTPUT.PUT_LINE('	#### Lema              -   Votos Si  /  %  -  Votos en blanco  / %  - ');
	FOR detalle IN listasFiscales LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || detalle.lemaLista || ' - ' || detalle.votosSi || ' / ' || (detalle.votosSi/TOTAL_VOTOS_FISCALES)
			|| ' % - ' || detalle.votosEnBlanco || ' / ' || (detalle.votosEnBlanco/TOTAL_VOTOS_FISCALES) || ' % -  ###');
	END LOOP;
COMMIT;
END MostrarResumenVotacionD;



CREATE OR REPLACE PROCEDURE MostrarResumenSeleccionM(nombreSeleccion IN VARCHAR, fechaFin IN DATE) AS
	TOTAL_VOTOS NUMBER;
	CURSOR mociones IS
		SELECT m.nombre AS nombre , m.votosSi AS votosSi, m.votosNo AS votosNo, m.votosEnBlanco AS votosEnBlanco
		FROM Mocion m, MocionPorSeleccionar ma
		WHERE ma.nombreSeleccion = nombreSeleccion AND m.nombre = ma.nombreMocion;
BEGIN 
	TOTAL_VOTOS := ObtenerTotalVotosSeleccion(nombreSeleccion);

	DBMS_OUTPUT.PUT_LINE('#### Seleccion : ' || nombreSeleccion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Nombre mocion - Votos aprovacion / %  - Votos de rechazo / % - Votos en blanco / % - ###');
	FOR mocion IN mociones LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || mocion.nombre || ' - ' || mocion.votosSi || ' / ' || (mocion.votosSi/TOTAL_VOTOS) 
			|| ' % - ' || mocion.votosNo || ' / ' || (mocion.votosNo/TOTAL_VOTOS)
			|| ' % - ' || mocion.votosEnBlanco || ' / ' || (mocion.votosEnBlanco/TOTAL_VOTOS) || ' % -  ###');
		INSERT INTO DetalleVotacionProcesada VALUES (nombreSeleccion, mocion.nombre, 0, 0, 0, 0, 0, 0, mocion.votosSi, mocion.votosNo, mocion.votosEnBlanco);
	END LOOP;
COMMIT;
END MostrarResumenSeleccionM;

CREATE OR REPLACE FUNCTION ObtenerTotalVotosSeleccion(nombreSeleccion IN VARCHAR) 
RETURN NUMBER AS
	CURSOR totalesVotos IS
		SELECT COALESCE((m.votosSi + m.votosNo + m.votosEnBlanco), 0) AS totalVotos
		FROM Mocion m, MocionPorSeleccionar ma
		WHERE ma.nombreSeleccion = nombreSeleccion AND m.nombre = ma.nombreMocion; 
BEGIN
	FOR total IN totalesVotos LOOP
		RETURN total.totalVotos;
	END LOOP;
	RETURN 0;
END ObtenerTotalVotosSeleccion;

CREATE OR REPLACE PROCEDURE MostrarResumenAprovacionM(nombreAprovacion IN VARCHAR, fechaFin IN DATE) AS
	CANTIDAD_VOTOS_SI NUMBER;
	CANTIDAD_VOTOS_NO NUMBER;
	CANTIDAD_VOTOS_BLANCO NUMBER;
	TOTAL_VOTOS NUMBER;
	CURSOR mociones IS
		SELECT m.nombre AS nombre , m.votosSi AS votosSi, m.votosNo AS votosNo, m.votosEnBlanco AS votosEnBlanco
		FROM Mocion m, MocionPorAprovar ma
		WHERE ma.nombreAprovacion = nombreAprovacion AND m.nombre = ma.nombreMocion;
BEGIN
	SELECT COALESCE(SUM(m.votosSi), 0) INTO CANTIDAD_VOTOS_SI
	FROM Mocion m, MocionPorAprovar ma
	WHERE ma.nombreAprovacion = nombreAprovacion AND ma.nombreMocion = m.nombre;
	
	SELECT COALESCE(SUM(m.votosNo), 0) INTO CANTIDAD_VOTOS_NO
	FROM Mocion m, MocionPorAprovar ma
	WHERE ma.nombreAprovacion = nombreAprovacion AND ma.nombreMocion = m.nombre;

	SELECT COALESCE(SUM(m.votosEnBlanco), 0) INTO CANTIDAD_VOTOS_BLANCO
	FROM Mocion m, MocionPorAprovar ma
	WHERE ma.nombreAprovacion = nombreAprovacion AND ma.nombreMocion = m.nombre;

	TOTAL_VOTOS := CANTIDAD_VOTOS_SI + CANTIDAD_VOTOS_NO + CANTIDAD_VOTOS_BLANCO;
	INSERT INTO VotacionProcesadaEnResumen VALUES (nombreAprovacion, TOTAL_VOTOS, 0, 0, 0);

	DBMS_OUTPUT.PUT_LINE('#### Aprovacion : ' || nombreAprovacion ||  ' - Fecha de cierre/publicacion : ' || TO_DATE(fechaFin, 'DD-MM-YYYY'));
	DBMS_OUTPUT.PUT_LINE('	#### Nombre mocion - Votos por si / %  - Votos de rechazo / % - Votos en blanco / % - ###');
	FOR mocion IN mociones LOOP
		DBMS_OUTPUT.PUT_LINE('	#### ' || mocion.nombre || ' - ' || mocion.votosSi || ' / ' || (mocion.votosSi/TOTAL_VOTOS) 
			|| ' % - ' || mocion.votosNo || ' / ' || (mocion.votosNo/TOTAL_VOTOS)
			|| ' % - ' || mocion.votosEnBlanco || ' / ' || (mocion.votosEnBlanco/TOTAL_VOTOS) || ' % -  ###');
		INSERT INTO DetalleVotacionProcesada VALUES (nombreAprovacion, mocion.nombre, 0, 0, 0, 0, 0, 0, mocion.votosSi, mocion.votosNo, mocion.votosEnBlanco);
	END LOOP;
COMMIT;
END MostrarResumenAprovacionM;
