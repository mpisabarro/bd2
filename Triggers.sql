--TRIGGERS----------------------------------------------------------------------------------------------------------------------
--1
CREATE OR REPLACE TRIGGER maxIntegrantesElectoral BEFORE INSERT OR UPDATE ON IntegranteComisionElectoral
    DECLARE cantIntegrantes INT;
    BEGIN		
        SELECT COUNT (*) INTO cantIntegrantes
        FROM IntegranteComisionElectoral;
		IF (cantIntegrantes = 10)
            THEN RAISE_APPLICATION_ERROR(-20001,'Como máximo se permiten 10 integrantes en la comisión electoral.');
		END IF;
	END;
 /  
--2
CREATE OR REPLACE TRIGGER maxIntegrantesFiscal BEFORE INSERT OR UPDATE ON IntegranteComisionFiscal
    DECLARE cantIntegrantes INT;
    BEGIN		
        SELECT COUNT (*) INTO cantIntegrantes
        FROM IntegranteComisionFiscal;
		IF (cantIntegrantes = 10)
            THEN RAISE_APPLICATION_ERROR(-20001,'Como máximo se permiten 10 integrantes en la comisión fiscal.');
		END IF;
	END;
/

--3
CREATE OR REPLACE TRIGGER maxIntegrantesDirectiva BEFORE INSERT OR UPDATE ON IntegranteComisionDirectiva
    DECLARE cantIntegrantes INT;
    BEGIN		
        SELECT COUNT (*) INTO cantIntegrantes
        FROM IntegranteComisionDirectiva;
		IF (cantIntegrantes = 10)
            THEN RAISE_APPLICATION_ERROR(-20001,'Como máximo se permiten 10 integrantes en la comisión directiva.');
		END IF;
	END;
/

--4
CREATE OR REPLACE TRIGGER cantIntegrantesComision BEFORE INSERT OR UPDATE ON VotacionDirectiva
    FOR EACH ROW
    DECLARE 
    cantIntegrantesE INT;
    cantIntegrantesF INT;
    cantIntegrantesD INT;
    BEGIN
        SELECT COUNT (*) INTO cantIntegrantesE
        FROM IntegranteComisionElectoral 
        WHERE lema = :new.listaElectoral;
        SELECT COUNT (*) INTO cantIntegrantesF
        FROM IntegranteComisionFiscal
        WHERE lema = :new.listaFiscal;
        SELECT COUNT (*) INTO cantIntegrantesD
        FROM IntegranteComisionElectoral 
        WHERE lema = :new.listaDirectiva;  
        IF (cantIntegrantesE < 10 OR cantIntegrantesF < 10 OR cantIntegrantesD < 10)
            THEN RAISE_APPLICATION_ERROR(-20001,'No hay suficientes integrantes en la comisión.');
		END IF;       
    END;

--5
--Una persona no puede votar dos veces en una misma votacion
CREATE OR REPLACE TRIGGER checkVotacionDoble BEFORE INSERT OR UPDATE ON ParticipacionVotante
    FOR EACH ROW
    DECLARE
    yaVoto INT;
    BEGIN
        SELECT COUNT (*) INTO yaVoto
        FROM ParticipacionVotante WHERE (numeroAfiliado = :new.numeroAfiliado AND nombreVotacion = :new.nombreVotacion); 
        IF (yaVoto <> 0) THEN RAISE_APPLICATION_ERROR(-20001, 'El votante ya ha participado de la votación.');
		END IF;
    END;        
/

--7
--solamente puede estar habilitada una votacion a la vez
CREATE OR REPLACE TRIGGER checkVotacionHabilitada BEFORE INSERT OR UPDATE ON Votacion
    FOR EACH ROW
    DECLARE
    cantHabilitada INT;
    BEGIN		
        SELECT COUNT (*) INTO cantHabilitada
        FROM Votacion
        WHERE estado = 'HABILITADA';
        IF (cantHabilitada > 0 )
            THEN RAISE_APPLICATION_ERROR(-20001,'Solamente puede estar habilitada una votación a la vez.');
        END IF;
	END;
 /  
 
--8
--Si usuarioUltimaModificacion es admin, no se puede votar
--Si el votante se encuentra atrasado con el pago de la cuota, no puede votar
CREATE OR REPLACE TRIGGER checkVotanteAtrasado BEFORE INSERT ON ParticipacionVotante
    FOR EACH ROW
    DECLARE  
    pagoCuota INT;
    BEGIN		
        ValidarUltimaMod(:new.numeroAfiliado);
        SELECT COUNT (*) INTO pagoCuota
        FROM Cuota c 
		WHERE c.numeroAfiliado = :new.numeroAfiliado
        AND c.fecha > ADD_MONTHS(SYSDATE, -3);       
		IF (pagoCuota = 0)
            THEN RAISE_APPLICATION_ERROR(-20001,'El votante no tiene permitido votar dado que se encuentra atrasado con el pago de la cuota.');
		END IF;
	END;
/  
CREATE OR REPLACE PROCEDURE ValidarUltimaMod(nroAfiliado IN INT)
IS
modEsVotante INT;
nombreUser VARCHAR(255);
BEGIN 
    SELECT v.nombreUsuario INTO nombreUser
    FROM Votante v
    WHERE v.numeroAfiliado = nroAfiliado;
    SELECT DISTINCT COUNT(*) INTO modEsVotante
    FROM Votante v
    WHERE v.usuarioUltimaModificacion = nombreUser;
    IF (modEsVotante = 0)
        THEN RAISE_APPLICATION_ERROR(-20001,'La ultima modifiacion ha sido por un administrador.');
    END IF;
END ValidarUltimaMod;
 
--9
--cuando se inserta un votante por primera vez, el mismo debe estar deshabilitado
--Unicamente los administradores pueden crear votantes
CREATE OR REPLACE TRIGGER checkUsuarioEsAdmin BEFORE INSERT ON Votante
    FOR EACH ROW
    DECLARE
    esAdmin INT;
    BEGIN
        :new.afiliado := 0;
        SELECT COUNT (*) INTO esAdmin
        FROM Administrativo
        WHERE nombreUsuario = :new.usuarioUltimaModificacion;
        IF (esAdmin = 0)
            THEN RAISE_APPLICATION_ERROR(-20001,'Unicamente los administradores pueden crear votantes.');
        END IF;
    END;
   
--10
--Si la votacion esta cerrada o publicada no puede volver a abrirse o ser borrador
CREATE VIEW Votaciones AS
SELECT nombre, fechaInicio, fechaFin, estado
FROM Votacion;
CREATE OR REPLACE TRIGGER checkVotacionCoP INSTEAD OF UPDATE ON Votaciones
    FOR EACH ROW
    DECLARE
    cantCoP INT :=0;
    BEGIN	        
        SELECT COUNT (*) INTO cantCoP
        FROM Votacion
        WHERE ((nombre = :new.nombre) AND (estado = 'CERRADA' OR estado ='PUBLICADA'));
        IF (cantCoP > 0)
            THEN IF ((:new.estado = 'HABILITADA') OR (:new.estado = 'BORRADOR'))
                 THEN RAISE_APPLICATION_ERROR(-20001,'Una votación CERRADA o  PUBLICADA no puede pasar a tener estado HABILITADA o BORRADOR.');
            END IF;
        END IF;
	END;
 /   
 
--11
 --Al insertar una cuota, si la misma cubre el atraso, habilitar al votante
CREATE VIEW Cuotas AS
SELECT numeroAfiliado, fecha
FROM Cuota;
CREATE OR REPLACE TRIGGER cubreAtraso INSTEAD OF INSERT ON Cuotas
    FOR EACH ROW
    DECLARE
    cubre INT;
    BEGIN   
        SELECT COUNT (*) numeroAfiliado INTO cubre
        FROM Cuota
        WHERE ((ADD_MONTHS(:new.fecha, 3) >= SYSDATE) AND (numeroAfiliado = :new.numeroAfiliado));
        IF (cubre > 0)
            THEN UPDATE Votante SET afiliado = 1 WHERE numeroAfiliado = :new.numeroAfiliado;
        END IF;
    END;
/
	
--12
--al modificar datos de votante, se debe actualizar usuarioUltimaModificacion
--al modificar datos de votante, se requiere registrar quien hizo el cambio, la fecha y la hora
CREATE OR REPLACE TRIGGER ModificacionVotante AFTER UPDATE ON Votante
    FOR EACH ROW
    DECLARE
    esAdmin INT;
    BEGIN
        SELECT COUNT (*) INTO esAdmin 
        FROM Administrativo
        WHERE nombreUsuario = :new.usuarioUltimaModificacion;
        IF ((:new.usuarioUltimaModificacion = :old.nombreUsuario) OR (esAdmin > 0))
            THEN INSERT INTO HistoricoABMVotante VALUES (:new.numeroAfiliado, :new.usuarioUltimaModificacion, SYSDATE);                 
        ELSE 
            RAISE_APPLICATION_ERROR(-20001,'Un votante solo puede ser modificado por un administrador o por el mismo.');
            ROLLBACK;
        END IF;
    END;
/ 

--14
--cuando se inserta una votacion la misma por defecto tiene estado BORRADOR
CREATE OR REPLACE TRIGGER setEstadoBorrador BEFORE INSERT ON Votacion
    FOR EACH ROW
    BEGIN
        :new.estado := 'BORRADOR';
    END;
/
--15
--al modificar un votante, dependiendo de la accion se afilia o no al mismo
CREATE OR REPLACE TRIGGER updateContrasena AFTER UPDATE ON Credenciales
    FOR EACH ROW
    DECLARE
    nroAfiliado INT;
    BEGIN
        IF(:old.contrasena <> :new.contrasena)
            THEN 
                 SELECT numeroAfiliado INTO nroAfiliado
                 FROM Votante 
                 WHERE nombreUsuario = :new.nombreUsuario;
                 HabilitarAfiliado(nroAfiliado, :new.nombreUsuario);
        END IF;
    END;   
/
CREATE OR REPLACE PROCEDURE HabilitarAfiliado(numeroAfiliado IN VARCHAR, nombreUsuario IN VARCHAR)
IS
desafiliado INT;
cuotas INT;
BEGIN
    SELECT  COUNT (*) INTO desafiliado
    FROM Desafiliacion d
    WHERE d.numeroAfiliado = numeroAfiliado;
    --SI EXISTE DESAFILIACION
    IF (desafiliado > 0)      
        THEN SELECT COUNT (*) INTO cuotas
             FROM Cuota c, Desafiliacion d
             WHERE c.fecha > d.fecha ;
             IF (cuotas > 0)
                THEN UPDATE Votante SET afiliado = 1 WHERE nombreUsuario = nombreUsuario;
             END IF;
     --SI NO EXISTE DESAFILIACION
     ELSE 
        SELECT COUNT (*) INTO cuotas
        FROM Cuota c
        WHERE ((c.numeroAfiliado = numeroAfiliado) AND (fecha > ADD_MONTHS(SYSDATE, -3)));
        IF (cuotas > 0)
            THEN UPDATE Votante SET afiliado = 1 WHERE nombreUsuario = nombreUsuario;
        END IF;
    END IF; 
END HabilitarAfiliado;

--16
--Luego de insertar en seleccionMociones, en Mocion se deben actualizar que listas se votaron y cuales no
DROP TRIGGER listasVotadas;
CREATE VIEW Mociones AS
SELECT nombre, descripcion, votosSi, votosNo, votosEnBlanco
FROM Mocion;
CREATE OR REPLACE TRIGGER listasVotadas INSTEAD OF UPDATE ON Mociones
FOR EACH ROW
DECLARE
perteneceSeleccion INT;
BEGIN
    SELECT COUNT (*) INTO perteneceSeleccion
    FROM SeleccionMociones
    WHERE nombreSeleccion = :new.nombre;
    IF(perteneceSeleccion <> 0)
        THEN ActualizarNoVotadas(:old.nombre);
    END IF;
END;
/
CREATE OR REPLACE PROCEDURE ActualizarNoVotadas(mocionVotada IN VARCHAR)
IS
CURSOR c1 IS
    SELECT m.nombre
    FROM Mocion m
    WHERE m.nombre <> mocionVotada;
BEGIN    
    UPDATE Mocion SET votosSi = votosSi + 1 WHERE nombre = mocionVotada;
    FOR moc IN c1 
    LOOP
        UPDATE Mocion SET votosNo = votosNo + 1 WHERE nombre = moc.nombre;
    END LOOP;    
END ActualizarNoVotadas;

--17
--Aprobacion de mociones puede tener hasta 10 mociones
CREATE OR REPLACE TRIGGER maxAprobacionMociones BEFORE INSERT ON MocionPorAprovar
    FOR EACH ROW
    DECLARE
    cantMociones INT;
    BEGIN
        SELECT COUNT (*) INTO cantMociones
        FROM MocionPorAprovar;
        IF(cantMociones = 10)
            THEN RAISE_APPLICATION_ERROR(-20001,'Como máximo se permiten 10 mociones.');
        END IF;
    END;
 / 

--18 
--Se provee un servicio para permitir la aprovacion de mociones
--Cada voto pertenece a  ('SI', 'NO', 'BLANCO')
CREATE OR REPLACE PROCEDURE AprovarMociones(numeroAfiliado IN VARCHAR, nombreAprovacion IN VARCHAR, 
    voto1 IN VARCHAR, nombreMocion1 IN VARCHAR, voto2 IN VARCHAR, nombreMocion2 IN VARCHAR, 
    voto3 IN VARCHAR, nombreMocion3 IN VARCHAR, voto4 IN VARCHAR, nombreMocion4 IN VARCHAR, 
    voto5 IN VARCHAR, nombreMocion5 IN VARCHAR, voto6 IN VARCHAR, nombreMocion6 IN VARCHAR,
    voto7 IN VARCHAR, nombreMocion7 IN VARCHAR, voto8 IN VARCHAR, nombreMocion8 IN VARCHAR, 
    voto9 IN VARCHAR, nombreMocion9 IN VARCHAR, voto10 IN VARCHAR, nombreMocion10 IN VARCHAR)
AS
    YA_VOTO NUMBER;
    EXISTE_APROVACION NUMBER;
    TRUE NUMBER;
    FALSE NUMBER;
BEGIN
    SELECT COUNT(*) INTO YA_VOTO
    FROM ParticipacionVotante p
    WHERE p.numeroAfiliado = numeroAfiliado AND p.nombreVotacion = nombreAprovacion;

    SELECT COUNT(*) INTO EXISTE_APROVACION
    FROM AprovacionMociones a
    WHERE a.nombreAprovacion = nombreAprovacion;

    TRUE := 1;

    IF YA_VOTO = TRUE
    THEN
        DBMS_OUTPUT.PUT_LINE('ERROR: El votante ya ha participado en la votacion.');
    ELSE
        IF EXISTE_APROVACION = TRUE
        THEN
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion1, voto1);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion2, voto2);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion3, voto3);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion4, voto4);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion5, voto5);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion6, voto6);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion7, voto7);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion8, voto8);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion9, voto9);
            SumarVotoEnAprovacion(nombreAprovacion, nombreMocion10, voto10);
            INSERT INTO ParticipacionVotante VALUES (numeroAfiliado, nombreAprovacion);
            COMMIT;
        ELSE
            DBMS_OUTPUT.PUT_LINE('ERROR: La aprobacion seleccionada no existe.');
        END IF;
        
    END IF; 
END AprovarMociones;

--19
--Aprobacion de mociones puede tener hasta 10 mociones
CREATE OR REPLACE TRIGGER maxAprobacionMociones BEFORE INSERT ON MocionPorAprovar
    FOR EACH ROW
    DECLARE
    cantMociones INT;
    BEGIN
        SELECT COUNT (*) INTO cantMociones
        FROM MocionPorAprovar;
        IF(cantMociones = 10)
            THEN RAISE_APPLICATION_ERROR(-20001,'Como máximo se permiten 10 mociones.');
        END IF;
    END;
 / 



 --Procedure auxiliar para la aprovacion de mociones
CREATE OR REPLACE PROCEDURE SumarVotoEnAprovacion(nombreAprovacion IN VARCHAR, nombreMocion IN VARCHAR, voto IN VARCHAR) AS
    CANTIDAD_VOTOS_SI NUMBER;
    CANTIDAD_VOTOS_NO NUMBER;
    CANTIDAD_VOTOS_BLANCO NUMBER;
BEGIN 
   SELECT m.votosSi INTO CANTIDAD_VOTOS_SI
   FROM Mocion m
   WHERE m.nombre = nombreMocion;

   SELECT m.votosNo INTO CANTIDAD_VOTOS_NO
   FROM Mocion m
   WHERE m.nombre = nombreMocion;

   SELECT m.votosEnBlanco INTO CANTIDAD_VOTOS_BLANCO
   FROM Mocion m
   WHERE m.nombre = nombreMocion;   

   IF voto = 'SI'
   THEN 
     UPDATE Mocion SET votosSi = CANTIDAD_VOTOS_SI + 1 WHERE nombre = nombreMocion;
   END IF;

   IF voto = 'NO'
   THEN
    UPDATE Mocion SET votosNo = CANTIDAD_VOTOS_NO + 1 WHERE nombre = nombreMocion;
   END IF;

   IF voto = 'BLANCO'
   THEN
    UPDATE Mocion SET votosEnBlanco = CANTIDAD_VOTOS_BLANCO + 1 WHERE nombre = nombreMocion;
   END IF;
COMMIT;
EXCEPTION
   WHEN OTHERS
   THEN NULL;-- si hay una excepcion se la deja pasar, porque se asume que para esta aprovacion no hay 10 mociones
END SumarVotoEnAprovacion;